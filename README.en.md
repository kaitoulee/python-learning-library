# python资料学习库

#### Description
每一个程序员都有一个愿景，希望时代更好，希望我们能建设世界，这个是开源的python资料库，希望你能有一份助力，我们会把优秀的书籍，文档，上传这个库，同时我们坚持和捍卫正版主义，有能力，有条件，在获得书籍后，看可以去购买原版书籍，如果有侵权请联系我进行删除，再次进行感谢

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
